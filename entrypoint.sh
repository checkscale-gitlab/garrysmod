#!/bin/bash
source ./fileToEnv.sh;

# Set the directory of the games 
gameDir="$STEAM_DIR/games"

# Install CS:S Dedicated Server
$STEAM_DIR/steamcmd/steamcmd.sh \
  +login anonymous \
  +force_install_dir $gameDir \
  +app_update 232330 validate \
  +quit;

# Install Garry's Mod Dedicated Server
$STEAM_DIR/steamcmd/steamcmd.sh \
  +login anonymous \
  +force_install_dir $gameDir \
  +app_update 4020 -beta x86-64 validate \
  +quit;

# Create mount.cfg
cat > $gameDir/garrysmod/cfg/mount.cfg << EOF
"mountcfg"
{
	"cstrike"	"$gameDir/cstrike"
}
EOF

# Set environment variables from files
setEnvFromFile "RCON_PASSWORD";
setEnvFromFile "PASSWORD";
setEnvFromFile "GSLT";
setEnvFromFile "WORKSHOP_COLLECTION";
setEnvFromFile "ADDITIONAL_ARGS";
setEnvFromFile "TICKRATE";
setEnvFromFile "GAMEMODE";
setEnvFromFile "MAX_PLAYERS";
setEnvFromFile "MAP";
setEnvFromFile "HOSTNAME";

# Set up optional arguments
optionalArguments="";
if [ -v WORKSHOP_COLLECTION ]; then
  optionalArguments="+host_workshop_collection $WORKSHOP_COLLECTION"
fi;

if [ -v GSLT ]; then
  optionalArguments="$optionalArguments +sv_setsteamaccount $GSLT"
fi;

if [ -v RCON_PASSWORD ]; then
  optionalArguments="$optionalArguments +rcon_password $RCON_PASSWORD"
fi;

if [ -v PASSWORD ]; then
  optionalArguments="$optionalArguments +sv_password $PASSWORD"
fi;

if [ -v ADDITIONAL_ARGS ]; then
  optionalArguments="$optionalArguments $ADDITIONAL_ARGS"
fi;

# Start up server
$gameDir/srcds_run_x64 \
  -game garrysmod \
  -tickrate $TICKRATE \
  -usercon \
  +gamemode $GAMEMODE \
  +maxplayers $MAX_PLAYERS \
  +map $MAP \
  +hostname $HOSTNAME \
  $optionalArguments;