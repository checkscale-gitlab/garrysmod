FROM cm2network/steamcmd:latest

ENV STEAM_DIR /home/steam

ENV GAMEMODE sandbox
ENV MAX_PLAYERS 16
ENV MAP gm_flatgrass
ENV HOSTNAME "Garry's Mod"
ENV TICKRATE 66

WORKDIR /home/steam/
VOLUME [ "/home/steam" ]

COPY entrypoint.sh .
COPY mods/fileToEnv.sh .

EXPOSE 27015/tcp 27015/udp 27020/udp 27005/udp 26900/udp

ENTRYPOINT [ "bash", "./entrypoint.sh" ]